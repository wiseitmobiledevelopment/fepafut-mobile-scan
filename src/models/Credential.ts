export class Credential {
  id: number;
  requesterId: number;
  statusId: number;
  name: string;
  lastname: string;
  document: string;
  organization: string;
  position: string;
  token: string;
  zoneId: string;
  zone: string;
  status: string;
  scanned_by: string;
  scanned_at: Date
}
