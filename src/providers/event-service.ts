import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Event} from '../models/Event'
import {Device} from 'ionic-native';
import {NavController, AlertController} from "ionic-angular";

@Injectable()
export class EventService {
    selected: Event;
    deviceStatus:any;

    constructor(public http: Http,
                public navCtrl: NavController,
                private alertCtrl: AlertController) {
        this.deviceStatus ={label: "No Autonizado",authorized:false}
    }

    public getEvents(): Promise<Event[]> {
        return this.http.get("https://credenciales.fepafut.com/mob_apis/getEvents.php?uuid="+Device.uuid)
            .toPromise()
            .then(response => {
                if(response.json().error){
                    this.handleError(response.json());
                }else {
                    this.deviceStatus ={label: "Autorizado",authorized:true};
                    return response.json().data as Event[]
                }
            })
            .catch(this.handleError)
    }

    private handleError(error: any): Promise<any> {
        this.deviceStatus ={label: "No Autorizado",authorized:false};
        this.navCtrl.push(this.alertCtrl.create({
            title: "ERROR",
            subTitle: error.message,
            buttons: ["Close"]
        }));

        return Promise.reject(error);
    }
}
