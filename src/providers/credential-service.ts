import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Credential} from '../models/Credential';
import {Device} from 'ionic-native';

@Injectable()
export class CredentialService {
    selected: Credential;

    constructor(public http: Http) {
    }

    public getCredentials(id: number): Promise<Credential[]> {
        return this.http.get("https://credenciales.fepafut.com/mob_apis/getCredentials.php?uuid=" + Device.uuid + "&eventId=" + id)
            .toPromise()
            .then(response => {
                if (response.json().error) {
                    this.handleError(response.json());
                } else {
                    return response.json().data as Credential[]
                }
            })
            .catch(this.handleError)
    }

    public postLogs(logs: any[]): Promise<any> {
        return this.http.post("https://credenciales.fepafut.com/mob_apis/postLogs.php",logs)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError)
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error);
    }

}
