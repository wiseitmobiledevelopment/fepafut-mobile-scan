import { Component } from '@angular/core';
import {Platform, ToastController, AlertController} from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import {StartPage} from '../pages/start/start';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage = StartPage;

  constructor(public platform: Platform,public alertCtrl: AlertController,public toastCtrl:ToastController) {
    platform.ready().then(()=>{
      platform.registerBackButtonAction(()=>this.myHandlerFunction());
      StatusBar.styleDefault();
      Splashscreen.hide();
    })
  }

  myHandlerFunction(){
    this.showAlert();
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: '¿Desea Salir?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            alert =null;
          }
        },
        {
          text: 'Salir',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();
  }

}
