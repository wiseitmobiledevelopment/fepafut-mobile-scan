import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { StartPage } from '../pages/start/start';
import { ListPage } from '../pages/list/list';
import {DetailsPage} from "../pages/details/details";
import {SearchPage} from "../pages/search/search";
import {ErrorPage} from "../pages/errorPage/errorPage";
import {ResponsePage} from "../pages/response/response";
import {StatusPipe} from '../pipes/credential-status-pipe';
import {SearchPipe} from '../pipes/credential-search-pipe';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    StartPage,
    ListPage,
    DetailsPage,
    SearchPage,
    ResponsePage,
    ErrorPage,
    StatusPipe,
    SearchPipe,
  ],
  imports: [
    IonicModule.forRoot(MyApp)

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    StartPage,
    ListPage,
    DetailsPage,
    SearchPage,
    ResponsePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
