import {Component} from '@angular/core';
import {Event} from '../../models/Event';
import {EventService} from "../../providers/event-service";
import {DetailsPage} from "../details/details";
import {NavController, NavParams} from "ionic-angular";
import {Credential} from "../../models/Credential";

@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
    providers: [EventService]
})
export class SearchPage {
    event: Event;
    credentials: Credential[];
    filter:any;

    constructor(public navCtrl: NavController,
                private navParams: NavParams) {

        this.event = navParams.get("event");
        this.credentials = navParams.get("credentials");
        this.filter = "";

    }

    detailPage(cred: Credential) {
        console.log("PARAM: ", cred);
        this.navCtrl.push(DetailsPage,{
            event: this.event,
            credential:cred
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SearchPage');
    }

}
