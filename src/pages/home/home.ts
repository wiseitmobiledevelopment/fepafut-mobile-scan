import {Component} from '@angular/core';

import {NavController, AlertController, ModalController, Platform} from 'ionic-angular';
import {Event} from '../../models/Event';
import {Credential} from '../../models/Credential';
import {EventService} from '../../providers/event-service';
import {CredentialService} from '../../providers/credential-service';
import {ListPage} from "../list/list";
import {DetailsPage} from '../details/details';
import {SearchPage} from "../search/search";
import {StartPage} from "../start/start";
import {ResponsePage} from '../response/response';
import {BarcodeScanner, Network, Device} from 'ionic-native';
import * as PouchDB from 'pouchdb';
import * as moment from 'moment';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
    providers: [EventService, CredentialService]
})
export class HomePage {
    events: Event[];
    evenSelected: Event;

    credentials: Credential[];
    credentialsB: Credential[];

    filtro: any;
    model: any;
    private _dbEvents;
    private _dbCredentials;
    _dbCredentialsLogs;

    constructor(public navCtrl: NavController,
                public modalCtrl: ModalController,
                private alertCtrl: AlertController,
                private eventService: EventService,
                private credentialService: CredentialService,
                public platform: Platform) {

        this.platform = platform;
        this.navCtrl = navCtrl;
        this.model = {event: null};
        this.events = new Array<Event>();
        this.credentials = new Array<Credential>();
        this.credentialsB = new Array<Credential>();
        this._dbEvents = new PouchDB('events', {adapter: 'websql'});
        this._dbCredentials = new PouchDB('credentials', {adapter: 'websql'});
        this._dbCredentialsLogs = new PouchDB('credentialsLogs', {adapter: 'websql'});
        this.filtro = "4";
    }

    doRefresh(refresher) {

        if (Network.type !== 'unknown' && Network.type !== 'none' && this.events.length > 0) {
            this.credentialService.getCredentials(this.evenSelected.id)
                .then(credentials => {
                    if (credentials) {
                        //console.log("destroying db");
                        this.credentials = credentials;
                        //console.log("credentials:", credentials.length);
                        //this.credentials = credentials;
                        this._dbCredentials.destroy().then(res =>{
                            //console.log("recreating db");
                            this._dbCredentials = new PouchDB('credentials', {adapter: 'websql'});
                            //console.log("updating db");
                            this._dbCredentials.bulkDocs(this.credentials);

                        })
                    }else{
                        refresher.complete()
                    }
                })
                .catch(refresher.complete())
        } else {
            setTimeout(() => {
                refresher.complete();
            }, 2000);
        }
    }

    navigationLink() {
        this.navCtrl.push(ListPage);
    }

    detailPage(cred: Credential) {
        if (this.evenSelected) {
            this.navCtrl.push(DetailsPage, {
                event: this.evenSelected,
                credential: cred
            })
        }
    }

    searchPage() {
        if (this.evenSelected) {
            this.navCtrl.push(SearchPage, {
                event: this.evenSelected,
                credentials: this.credentials
            });
        }
    }

    ngOnInit(): void {
        this.platform.ready().then(() => {

            return this._dbEvents.allDocs({include_docs: true})
                .then(docs => {
                    //console.log("LOCAL EVENTS: ", docs);
                    this.events = docs.rows.map(row => {
                        return row.doc;
                    });

                    this._dbEvents.changes({live: true, since: 'now', include_docs: true})
                        .on('change', this.onEventsChange);

                    if (Network.type !== 'unknown' && Network.type !== 'none' && this.events.length <= 0) {
                        this.eventService.getEvents().then(events => {
                            //console.log("EVENTS: ", events);
                            this.events = events;
                            if (this.events) {
                                this._dbEvents.bulkDocs(this.events);
                            }
                        }).catch(this.handleError)
                    }
                })
        })
    }

    getCredential(event: Event): void {
        if (event) {
            this.evenSelected = event;

            return this._dbCredentials.allDocs({include_docs: true})
                .then(docs => {
                    this.credentials = docs.rows.map(row => {
                        return row.doc;
                    });

                    this._dbCredentials.changes({live: true, since: 'now', include_docs: true})
                        .on('change', this.onCredentialsChange);

                    if (Network.type !== 'unknown' && Network.type !== 'none' && this.credentials.length <= 0) {
                        this.credentialService.getCredentials(event.id)
                            .then(credentials => {
                                this.credentials = credentials;
                                if (this.credentials) {
                                    this._dbCredentials.bulkDocs(this.credentials);
                                }
                            })
                    }
                    this.filtro = "4";
                    return this.credentials;
                })
        }
    }

    uploadLogs(): void {

        //console.log("UPLOADING");
        this._dbCredentialsLogs.allDocs({include_docs: true})
            .then(docs => {
                var logs = docs.rows.map(row => {
                    return row.doc;
                });

                if (Network.type !== 'unknown' && Network.type !== 'none' && logs.length > 0) {
                    this.credentialService.postLogs(logs).then(response => {
                        if (response.error) {
                            this.navCtrl.push(this.alertCtrl.create({
                                title: "Atención!",
                                subTitle: "No se logro actualizar los registros",
                                buttons: ["Cerrar"]
                            }))
                        } else {
                            this.navCtrl.push(this.alertCtrl.create({
                                title: "Atención!",
                                subTitle: "Datos actualizados",
                                buttons: ["Cerrar"]
                            }));

                            this._dbCredentialsLogs.allDocs().then(docs => {
                                docs.rows.map(row => {
                                    return this._dbCredentialsLogs.remove(row.id, row.value.rev);
                                })
                            })
                        }
                    })
                } else {
                    this.navCtrl.push(this.alertCtrl.create({
                        title: "Atención!",
                        subTitle: "No hay datos a actualizar",
                        buttons: ["Cerrar"]
                    }))
                }
            })
    }

    updateCredential(token: String) {
        //console.log("TOKEN: ", token);
        if (this.credentials) {
            var cred = this.credentials.findIndex(item => {
                //return item.token == "75dba7b1-f505-11e5-9876-0401b2152f01"
                return item.token == token;
            });

            if (cred > -1) {
                var c = this.credentials[cred];
                var a = moment();
                //console.log("CRED: ", a);
                this._dbCredentialsLogs.post({
                    scannedAt: a,
                    scannedBy: Device.uuid,
                    id: c.token,
                    eventId: this.evenSelected.id
                });
                let modal = this.modalCtrl.create(ResponsePage, {credential: c});
                modal.present();


            } else {
                this.navCtrl.push(this.alertCtrl.create({
                    title: "Atención!",
                    subTitle: "Credencial no encontrada",
                    buttons: ["Cerrar"]
                }));
            }
        }
    }

    scan() {
        if (this.credentials) {
            BarcodeScanner.scan().then((barcodeData) => {
                this.updateCredential(barcodeData.text);
            }, (err) => {
                this.navCtrl.push(this.alertCtrl.create({
                    title: "Attention!",
                    subTitle: err,
                    buttons: ["Close"]
                }));
            });
        } else {
            this.navCtrl.push(this.alertCtrl.create({
                title: "Scan inactivo",
                subTitle: "No hay evento seleccionado o no hay credenciales para el evento",
                buttons: ["Close"]
            }));
        }
    }

    private onEventsChange = (change) => {
        var index = this.findIndex(this.events, change.id);
        var event = this.events[index];

        if (event && event.id === change.id) {
            this.events[index] = change.doc;
        } else {
            this.events.splice(index, 0, change.doc)
        }
    };

    private onCredentialsChange = (change) => {
        //console.log("CHANGE", change);
        var index = this.findIndex(this.credentials, change.id);
        var event = this.credentials[index];

        if (event && event.id === change.id) {
            this.credentials[index] = change.doc;
        } else {
            this.credentials.splice(index, 0, change.doc)
        }
    };

    private findIndex(array, id) {
        var low = 0, high = array.length, mid;
        while (low < high) {
            mid = (low + high) >>> 1;
            array[mid].id < id ? low = mid + 1 : high = mid
        }
        return low;
    }

    private handleError(error: any): void {

        this.navCtrl.push(this.alertCtrl.create({
            title: "ERROR",
            subTitle: error.message,
            buttons: ["Close"]
        }));

        this.navCtrl.push(StartPage);
    }
}
