import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {SearchPage} from '../search/search';
import {EventService} from "../../providers/event-service";
import {Event} from '../../models/Event';
import {Credential} from '../../models/Credential';
import {CredentialService} from "../../providers/credential-service";

@Component({
    selector: 'page-details',
    templateUrl: 'details.html',
    providers: [EventService, CredentialService]
})
export class DetailsPage {

    event: Event;
    credential: Credential;

    constructor(public navCtrl: NavController, private navParams: NavParams) {
        this.event = navParams.get("event");
        this.credential = navParams.get("credential");
    }

    searchPage(): void {
        this.navCtrl.push(SearchPage);
    }

    ionViewDidLoad(): void {
        console.log('ionViewDidLoad DetailsPage2');
        console.log("CRED: ", this.credential);
        console.log("EVENT: ", this.event);
    }

}
