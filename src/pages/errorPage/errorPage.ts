import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';

@Component({
    selector: 'page-response',
    templateUrl: 'errorPage.html'
})
export class ErrorPage {
    response: any;

    constructor(public viewCtrl: ViewController,
                private navParams: NavParams) {
        this.response = navParams.get("response");
    }


    dismiss() {
        this.viewCtrl.dismiss();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ResponsePage');
    }

}
