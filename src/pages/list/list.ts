import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DetailsPage } from '../details/details';
import { SearchPage } from "../search/search";
import { ModalController } from 'ionic-angular';
import { ResponsePage} from '../response/response';

/*
  Generated class for the List page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {}

  detailPage()
  {
    this.navCtrl.push(DetailsPage);
  }
  searchPage()
  {
    this.navCtrl.push(SearchPage);
  }

  presentModal(){
    let modal = this.modalCtrl.create(ResponsePage);
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPage');
  }

}
