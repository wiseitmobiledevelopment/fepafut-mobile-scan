import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';
import {Credential} from '../../models/Credential';
import {CredentialService} from "../../providers/credential-service";


@Component({
    selector: 'page-response',
    templateUrl: 'response.html',
    providers: [CredentialService]
})
export class ResponsePage {
    credential: Credential;

    constructor(public viewCtrl: ViewController,
                private navParams: NavParams) {
        this.credential = navParams.get("credential");

    }


    dismiss() {
        let data = {'foo': 'bar'};
        this.viewCtrl.dismiss(data);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ResponsePage');
    }

}
