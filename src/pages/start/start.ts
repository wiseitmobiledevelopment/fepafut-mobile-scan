import {Component} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {HomePage} from  '../home/home';
import {Device, Network} from 'ionic-native';
import {EventService} from '../../providers/event-service';
import * as PouchDB from 'pouchdb';

@Component({
    selector: 'page-start',
    templateUrl: 'start.html',
    providers: [EventService]
})
export class StartPage {

    device = Device.uuid;
    deviceStatus: any;
    private _dbEvents;

    constructor(public navCtrl: NavController,
                private eventService: EventService,
                public platform: Platform) {
        this.deviceStatus = this.eventService.deviceStatus;
        this.platform = platform;
        this._dbEvents = new PouchDB('events', {adapter: 'websql'});
    }

    GotoListLink() {
        if(this.deviceStatus.authorized) {
            this.navCtrl.push(HomePage);
        }
    }


    ngOnInit() {
        this.platform.ready().then(() => {

            return this._dbEvents.allDocs({include_docs: true})
                .then(docs => {

                    if (Network.type !== 'unknown' && Network.type !== 'none' && docs.rows.length <= 0) {
                        this.eventService.getEvents().then(res =>{
                            this.deviceStatus = this.eventService.deviceStatus;
                        })
                    }else{
                        this.deviceStatus ={label: "Autorizado",authorized:true}
                    }
                })
        })
    }

}
