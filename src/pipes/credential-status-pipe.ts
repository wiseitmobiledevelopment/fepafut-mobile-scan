import {Pipe} from '@angular/core';

@Pipe({
  name: 'StatusPipe'
})
export class StatusPipe {

  transform(value, args?) {
    if (args && value) {
      if(args == 4){
        return value
      }else {
        return value.filter(credential => {
          if (credential.statusId == args) {
            return credential;
          }
        })
      }
    } else {
      return null
    }
  }

}
