import {Pipe} from '@angular/core';

@Pipe({
    name: 'SearchPipe'
})
export class SearchPipe {

    transform(value, args?) {
        if (args && value) {
            args = args.toUpperCase();
            return value.filter(credential => {
                var name = credential.name.toUpperCase();
                var lastname = credential.lastname.toUpperCase();
                var organization = credential.organization.toUpperCase();

                if (name.indexOf(args) > -1 ||
                    lastname.indexOf(args) > -1 ||
                    organization.indexOf(args) > -1) {
                    return credential;
                }

            })

        } else {
            return value
        }
    }

}
